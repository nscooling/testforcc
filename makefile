CC = gcc
CFLAGS = -O
INCLUDES = -I./inc -I./Unity-master/src
LIBS = 
RM = rm
TARGET = pTest
AOBJS = src/counter.c src/person.c
TOBJS = tst/pTest.c tst/pTest_Runner.c ./Unity-master/src/unity.c
OBJS = $(AOBJS) $(TOBJS)

all: $(TARGET)

unity:
	ruby ./Unity-master/auto/generate_test_runner.rb tst/pTest.c

xml:
	ruby ./Unity-master/auto/parseOutput.rb -xml Output.txt

$(TARGET): unity $(OBJS) 
	$(CC) -o $@ $(OBJS) $(INCLUDES) 
	./$@ | tee testResults/Output.txt
	ruby ./Unity-master/auto/parseOutput.rb -xml testResults/Output.txt
	mv report.xml testResults/report.xml

$(AOBJS):
	$(CC) $(CFLAGS) $(INCLUDES) -c $(AOBJS)

$(TOBJS):
	$(CC) $(CFLAGS) $(INCLUDES) -c $(TOBJS)

clean:
	-$(RM) $(TARGET) 

.PHONY: clean all
