#include "unity.h"
#include "person.h"

PersonRef personRef;

void setUp(void)
{
	personRef = Person_personWithName("test tarou");
}
 
void tearDown(void)
{
	Person_dealloc(personRef);
}

 void testfullname(void)
{
	TEST_ASSERT_EQUAL_STRING("test tarou", Person_fullName(personRef));
}

 void testfirstname(void)
{
	TEST_ASSERT_EQUAL_STRING("test", Person_firstName(personRef));
}

 void testlastname(void)
{
	TEST_ASSERT_EQUAL_STRING("tarou", Person_lastName(personRef));
}

 void testsetfullname(void)
{
	Person_setFullName(personRef, "sample hanako");

	TEST_ASSERT_EQUAL_STRING("sample hanako", Person_fullName(personRef));
	TEST_ASSERT_EQUAL_STRING("sample", Person_firstName(personRef));
	TEST_ASSERT_EQUAL_STRING("hanako", Person_lastName(personRef));
}

 void testsetfirstname(void)
{
	Person_setFirstName(personRef, "sample");

	TEST_ASSERT_EQUAL_STRING("sample tarou", Person_fullName(personRef));
	TEST_ASSERT_EQUAL_STRING("sample", Person_firstName(personRef));
	TEST_ASSERT_EQUAL_STRING("tarou", Person_lastName(personRef));
}

 void testsetlastname(void)
{
	Person_setLastName(personRef, "hanako");

	TEST_ASSERT_EQUAL_STRING("test hanako", Person_fullName(personRef));
	TEST_ASSERT_EQUAL_STRING("test", Person_firstName(personRef));
	TEST_ASSERT_EQUAL_STRING("hanako", Person_lastName(personRef));
}

 void testnullcharfullname(void)
{
	Person_setFullName(personRef, "");

	TEST_ASSERT_EQUAL_STRING("", Person_fullName(personRef));
	TEST_ASSERT_EQUAL_STRING("", Person_firstName(personRef));
	TEST_ASSERT_EQUAL_STRING("", Person_lastName(personRef));
}

 void testnullpointerfullname(void)
{
	Person_setFullName(personRef, NULL);

	TEST_ASSERT_NULL(Person_fullName(personRef));
	TEST_ASSERT_NULL(Person_firstName(personRef));
	TEST_ASSERT_NULL(Person_lastName(personRef));
}

 void testnosepfullname(void)
{
	Person_setFullName(personRef, "sample");

	TEST_ASSERT_EQUAL_STRING("sample", Person_fullName(personRef));
	TEST_ASSERT_EQUAL_STRING("sample", Person_firstName(personRef));
	TEST_ASSERT_EQUAL_STRING("", Person_lastName(personRef));

	Person_setLastName(personRef, "tarou");
	TEST_ASSERT_EQUAL_STRING("sample tarou", Person_fullName(personRef));
	TEST_ASSERT_EQUAL_STRING("sample", Person_firstName(personRef));
	TEST_ASSERT_EQUAL_STRING("tarou", Person_lastName(personRef));

	Person_setFirstName(personRef, "test");
	TEST_ASSERT_EQUAL_STRING("test tarou", Person_fullName(personRef));
	TEST_ASSERT_EQUAL_STRING("test", Person_firstName(personRef));
	TEST_ASSERT_EQUAL_STRING("tarou", Person_lastName(personRef));
}