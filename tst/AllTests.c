#include <embUnit/embUnit.h>

TestRef CounterTest_tests(void);
TestRef PersonTest_tests(void);

int main (int argc, const char* argv[])
{
  if ( argc > 1 && 
       argv[1][0] == '-' &&
       argv[1][1] == 'X' )
  {
    TextUIRunner_setOutputter(XMLOutputter_outputter());
  
    TextUIRunner_start();
    TextUIRunner_runTest(CounterTest_tests());
    TextUIRunner_runTest(PersonTest_tests());
    TextUIRunner_end();
  }
  else
  {
    TestRunner_start();
    TestRunner_runTest(CounterTest_tests());
    TestRunner_runTest(PersonTest_tests());
    TestRunner_end();
  }
  return (int)0;
}
