CC = gcc
CFLAGS = -O
INCLUDES = inc
LIBS = 
RM = rm
TARGET = project.exe
OBJS = main.o counter.o person.o 

$(TARGET): $(OBJS)
	$(CC) -o $@ $(OBJS) 

all: $(TARGET) test

test:
	$(MAKE) -f makefile -B
	./alltests.exe

CruiseControl: $(TARGET) initResultFolder
	$(MAKE) -f makefile -B
	./alltests.exe -X > testResults/TEST-alltests.xml

initResultFolder:
	mkdir -p testResults
	rm -f testResults/*.*

.c.o:
	$(CC) $(CFLAGS) -I$(INCLUDES) -c src/$<

clean:
	-$(RM) $(TARGET) $(OBJS)

.PHONY: clean all

