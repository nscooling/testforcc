#include <stdio.h>
#include <strings.h>

#include "counter.h"
#include "person.h"


int main (int argc, const char* argv[])
{
  PersonRef listOfPersons[3];
  PersonRef* iterator;
  CounterRef counterOfMatches;

  counterOfMatches = Counter_counter();

  listOfPersons[0] = Person_personWithName( "Different Name" );
  listOfPersons[1] = Person_personWithName( "Matching Name" );
  listOfPersons[2] = (PersonRef) 0;

  iterator = &listOfPersons[0];

  while (*iterator != (PersonRef) 0)
  {
    if (strcmp( Person_firstName(*iterator), "Matching") == 0 &&
        strcmp( Person_lastName(*iterator), "Name")      == 0    )
    {
      Counter_inc( counterOfMatches );
    }
    *iterator++;
  }
  printf( "Number of matching names in the list: %d.\n", Counter_value( counterOfMatches ) );
  return 0;
}

